const BASE_URL = 'https://webbanhangapi.herokuapp.com/api'; //API url
const CITY = [
	'Hồ Chí Minh',
	'Hà Nội',
	'Cần Thơ',
	'Đà Nẵng',
	'Đồng Nai',
	'Tây Ninh',
	'Thừa Thiên Huế',
];
const DISTRICTS = [
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
	[
		'Quận 1',
		'Quận 2',
		'Quận 3',
		'Quận 4',
		'Quận 5',
		'Quận 6',
		'Quận 7',
		'Quận 8',
		'Quận 9',
		'Quận 10',
		'Quận 11',
		'Quận 12',
		'Tân Bình',
		'Gò Vấp',
		'Phú Nhuận',
	],
];
const PHONE_BRAND_LIST = [
	{ label: 'Apple', value: 0 },
	{ label: 'Samsung', value: 1 },
	{ label: 'Opple', value: 2 },
	{ label: 'Sony', value: 3 },
	{ label: 'Nokia', value: 4 },
	{ label: 'Vismarth', value: 5 },
	{ label: 'BKAV', value: 6 },
];
const CATEGORY_LIST = [
	{ label: 'Laptop', value: 0 },
	{ label: 'Smart Phone', value: 1 },
	{ label: 'TV', value: 2 },
	{ label: 'Smart Watch', value: 3 },
	{ label: 'Orthers', value: 4 },
];
const ITEMS_ORDER_LIST = [
	{ label: 'Price High To Low', value: 0 },
	{ label: 'Price Low To High', value: 1 },
	{ label: 'Date Arrival', value: 2 },
	{ label: 'Favorite', value: 3 },
];
const ITEM_COLORS = [
	{ label: 'Red', value: 0 },
	{ label: 'White', value: 1 },
	{ label: 'Green', value: 2 },
	{ label: 'Blue', value: 3 },
	{ label: 'Yellow', value: 4 },
];

export {
	BASE_URL,
	CITY,
	DISTRICTS,
	PHONE_BRAND_LIST,
	CATEGORY_LIST,
	ITEMS_ORDER_LIST,
	ITEM_COLORS,
};
