import Cart from './Cart';
import Home from './Home';
import SingleProduct from './SingleProduct';
import Contact from './Contact';
import ShopPage from './ShopPage';
import Category from './Category';
import Confirmation from './Confirmation';
import Checkout from './Checkout';
import PaymentMethod from './PaymentMethod';
import SignIn from './SignIn';
import SignUp from './SignUp';
import UserInfo from './UserInfo'
export {
	Cart,
	Category,
	Home,
	SingleProduct,
	ShopPage,
	Contact,
	Confirmation,
	Checkout,
	PaymentMethod,
	SignIn,
	SignUp,
	UserInfo
};
